package com.npci.bankproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.npci.bankproject.entity.TransactionEntity;

public interface TransactionRepository extends JpaRepository<TransactionEntity , Integer>{

}
