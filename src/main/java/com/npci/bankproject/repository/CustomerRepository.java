package com.npci.bankproject.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.npci.bankproject.entity.Customers;

@Repository
public interface CustomerRepository extends CrudRepository<Customers, Integer>{

	Customers getById(int cId);

}
