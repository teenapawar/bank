package com.npci.bankproject.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="transaction")
public class TransactionEntity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int trans_id;
	
	//private String name
	@Column(name="trans_amount")
	private float amount;
	
	@Column(name="from_c_id")
	private  int fromCId;
	
	@Column(name="to_c_id")
	private  int toCId;

	public int getTrans_id() {
		return trans_id;
	}

	public void setTrans_id(int trans_id) {
		this.trans_id = trans_id;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public int getFromCId() {
		return fromCId;
	}

	public void setFromCId(int fromCId) {
		this.fromCId = fromCId;
	}

	public int getToCId() {
		return toCId;
	}

	public void setToCId(int toCId) {
		this.toCId = toCId;
	}

	public TransactionEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TransactionEntity(int trans_id, float amount, int fromCId, int toCId) {
		super();
		this.trans_id = trans_id;
		this.amount = amount;
		this.fromCId = fromCId;
		this.toCId = toCId;
	}

	@Override
	public String toString() {
		return "TransactionEntity [trans_id=" + trans_id + ", amount=" + amount + ", fromCId=" + fromCId + ", toCId="
				+ toCId + "]";
	}


		

}
