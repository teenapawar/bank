package com.npci.bankproject.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="customers")
public class Customers implements Serializable {
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="c_id")
	private int customerId;
	
	@Column(name="npci_account")
	private String npci_account;
	
	@Column(name="name")
	private String name;
	
	public Customers() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Column(name="email")
	private String email;
	
	@Column(name="age")
	private int age;

	@Column(name="address")
	private String address;
	
	@Column(name = "acc_balance")
	private float ABalance;

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getNpci_account() {
		return npci_account;
	}

	public void setNpci_account(String npci_account) {
		this.npci_account = npci_account;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public float getABalance() {
		return ABalance;
	}

	public void setABalance(float aBalance) {
		ABalance = aBalance;
	}

	public Customers(int customerId, String npci_account, String name, String email, int age, String address,
			float aBalance) {
		super();
		this.customerId = customerId;
		this.npci_account = npci_account;
		this.name = name;
		this.email = email;
		this.age = age;
		this.address = address;
		ABalance = aBalance;
	}

	@Override
	public String toString() {
		return "Customers [customerId=" + customerId + ", npci_account=" + npci_account + ", name=" + name + ", email="
				+ email + ", age=" + age + ", address=" + address + ", ABalance=" + ABalance + "]";
	}




}
