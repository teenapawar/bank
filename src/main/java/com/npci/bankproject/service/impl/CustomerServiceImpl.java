package com.npci.bankproject.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.npci.bankproject.entity.Customers;
import com.npci.bankproject.exception.ResourceNotFoundException;
import com.npci.bankproject.repository.CustomerRepository;
import com.npci.bankproject.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService{

	@Autowired
	private CustomerRepository customerRepository;

	
	@Override
	public List<Customers> getAllCustomers() {
		// TODO Auto-generated method stub
		return (List<Customers>) customerRepository.findAll();
	}

	@Override
	public Customers getById(int customerId) throws Exception {
		// TODO Auto-generated method stub
		return customerRepository.findById(customerId).orElseThrow(()
				-> new ResourceNotFoundException("User not Found with id: " + customerId));
	}

	@Override
	public Customers addcustomer(Customers customer) {
		// TODO Auto-generated method stub
		return customerRepository.save(customer);
	}

	@Override
	public Customers deletecustomer(int customerId) throws Exception {
		Customers deletedUser = null;
        try {
            deletedUser = customerRepository.findById(customerId).orElse(null);
            if (deletedUser == null) {
                throw new Exception("No such user");
            }
            else {
                customerRepository.deleteById(customerId);
            }
        } catch (Exception ex) {
            throw ex;
        }
        return null;
	}

}
