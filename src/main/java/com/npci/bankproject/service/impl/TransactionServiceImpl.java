package com.npci.bankproject.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.npci.bankproject.entity.Customers;
import com.npci.bankproject.entity.TransactionEntity;
import com.npci.bankproject.repository.CustomerRepository;
//import com.npci.bankproject.repository.CustomerRepository;
import com.npci.bankproject.repository.TransactionRepository;
import com.npci.bankproject.service.TransactionService;

@Service
public class TransactionServiceImpl implements TransactionService {
	
	@Autowired
	private TransactionRepository transactionRepository;
	
	@Autowired
	private CustomerRepository customerRepository;
	
	@Override
	public List<TransactionEntity> getalltransaction() {
		// TODO Auto-generated method stub
		return (List<TransactionEntity>) transactionRepository.findAll();
	}
	
	@Override
	public TransactionEntity getTransById(int trans_id) {
		// TODO Auto-generated method stub
		return transactionRepository.findById(trans_id).orElse(null);
	}



	@Override
	public TransactionEntity addTransaction(TransactionEntity transactions) throws Exception{
		// TODO Auto-generated method stub
		int cId = transactions.getFromCId();

	       Customers entity = customerRepository.getById(cId);
			
			float customerBalance = entity.getABalance();
			float transAmount = transactions.getAmount();

			if (customerBalance >= transAmount) {

				// deduct sender customer balance
				entity.setABalance(customerBalance - transAmount);
				customerRepository.save(entity);
	       System.out.println(customerBalance - transAmount);
				// add target customer balance
		      int cId2 = transactions.getToCId();
		      

				Customers entity2 = customerRepository.getById(cId2);
				entity2.setABalance(customerBalance + transAmount);
				customerRepository.save(entity2);
			} 
			else {
				// return "Insufficient Balance"
				throw new Exception("Insufficient Balance, Can't perform Transaction. ");
			}
			return transactionRepository.save(transactions);
		}

}

	


