package com.npci.bankproject.service;

import java.util.List;

import com.npci.bankproject.entity.TransactionEntity;

public interface TransactionService {


	public List<TransactionEntity> getalltransaction();

	public TransactionEntity addTransaction(TransactionEntity transactions) throws Exception;

	TransactionEntity getTransById(int trans_id);

}
