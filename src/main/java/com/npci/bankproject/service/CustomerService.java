package com.npci.bankproject.service;

import java.util.List;

import org.springframework.stereotype.Service;

//import com.npci.bankproject.controller.Customer;
import com.npci.bankproject.entity.Customers;

@Service
public interface CustomerService {

	List<Customers> getAllCustomers();

	Customers getById(int customerId) throws Exception;

	//Customers addOrUpdate(Customers customer);

	Customers addcustomer(Customers customer);

	Customers deletecustomer(int customerId) throws Exception;

}
