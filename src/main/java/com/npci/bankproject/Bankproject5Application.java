package com.npci.bankproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Bankproject5Application {

	public static void main(String[] args) {
		SpringApplication.run(Bankproject5Application.class, args);
	}

}
